package com.samtulip.webscraper.repository;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.springframework.util.MimeTypeUtils.TEXT_PLAIN_VALUE;

/**
 * Tests for the WebScrapingPRoductRespository. 
 * 
 * Uses WireMock to serve html in a known state for the tests with the html
 * stored in files in the test resources for ease of use. currently the port 
 * is set as they follow up page is in the html, but if this is a problem it
 * can be extracted out and variable subsitition can be used to set port 
 * dynamically.
 * @author samtulip
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebScrapingProductRepository.class})
public class WebScrapingProductRepositoryTest {

    private final String page = "/1_product.html";
    private final String followUpPage = "/apricot.html";

    @Rule
    public final WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(10080));

    @Autowired
    private WebScrapingProductRepository repo;

    @Test
    public void testGetProducts() throws IOException {
        stubFor(get(urlEqualTo(page))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", TEXT_PLAIN_VALUE)
                        .withBody(IOUtils.toString(WebScrapingProductRepository.class.getResourceAsStream("/1_product.html"), StandardCharsets.UTF_8))));
        stubFor(get(urlEqualTo(followUpPage))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", TEXT_PLAIN_VALUE)
                        .withBody(IOUtils.toString(WebScrapingProductRepository.class.getResourceAsStream("/apricot.html"), StandardCharsets.UTF_8))));

        List<Product> expected = new ArrayList<>();
        expected.add(new Product("Sainsbury's Apricot Ripe & Ready x5", "Apricots", 35754, 350));
        List<Product> result = this.repo.getProducts("http://localhost:" + wireMockRule.port() + page);
        assertEquals("Products not scraped correctly", expected, result);
    }
    
    @Test( expected = IOException.class)
    public void testPageNotFound() throws IOException {
        this.repo.getProducts("http://localhost:" + wireMockRule.port() + "/NotFound");
    }
    
    @Test
    public void testWrongPage() throws IOException {
        stubFor(get(urlEqualTo(page))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", TEXT_PLAIN_VALUE)
                        .withBody(IOUtils.toString(WebScrapingProductRepository.class.getResourceAsStream("/apricot.html"), StandardCharsets.UTF_8))));

        List<Product> expected = new ArrayList<>();
        List<Product> result = this.repo.getProducts("http://localhost:" + wireMockRule.port() + page);
        assertEquals("Products not scraped correctly", expected, result);
    }

}
