package com.samtulip.webscraper;

import com.samtulip.webscraper.repository.Product;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author samtulip
 */
public class ProductJsonMapperTest {

    /**
     * Test of map method, of class ProductJsonMapper.
     */
    @Test
    public void testMap() {
        List<Product> products = new ArrayList<>();
        products.add(new Product("title", "description", 1024, 150));
        ProductJsonMapper instance = new ProductJsonMapper();
        JSONObject expected = new JSONObject();
        expected.put("total", 1.5);
        JSONArray array = new JSONArray();
        JSONObject value = new JSONObject();
        value.put("title","title");
        value.put("description","description");
        value.put("size",1);
        value.put("price",1.5d);
        array.add(value);
        expected.put("results", array);
        JSONObject result = instance.map(products);
        assertEquals(expected, result);
    }
    
}
