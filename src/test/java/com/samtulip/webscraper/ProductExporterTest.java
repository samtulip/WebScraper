/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samtulip.webscraper;

import com.samtulip.webscraper.repository.Product;
import com.samtulip.webscraper.repository.WebScrapingProductRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author samtulip
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ProductExporter.class, ProductJsonMapper.class})
public class ProductExporterTest {
    
    @Rule
    public OutputCapture outputCapture = new OutputCapture();
    
    @MockBean
    private WebScrapingProductRepository repo;
    
    @SpyBean
    private ProductJsonMapper mapper;
    
    @Autowired
    private ProductExporter exporter;
   
    @Test
    public void test() throws Exception {
        List<Product> products = new ArrayList<>();
        products.add(new Product("title", "description", 1024, 150));
        given(repo.getProducts(any())).willReturn(products);
        exporter.run("myPage");
        outputCapture.expect( Matchers.containsString("Getting products at myUrl\n{\"total\":1.5,\"results\":[{\"size\":1,\"price\":1.5,\"description\":\"description\",\"title\":\"title\"}]}"));
        verify(mapper).map(products);
    }
    
}
