package com.samtulip.webscraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main method and spring boot Application class
 * @author samtulip
 */
@SpringBootConfiguration
@ComponentScan("com.samtulip")
public class Application {
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
