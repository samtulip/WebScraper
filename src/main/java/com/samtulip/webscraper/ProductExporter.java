package com.samtulip.webscraper;

import com.samtulip.webscraper.repository.Product;
import com.samtulip.webscraper.repository.ProductRepository;
import java.util.List;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

/**
 * Spring command line runner taking the values entered at start up and controlling
 * the application.
 * @author samtulip
 */
@Service
public class ProductExporter implements CommandLineRunner {
    
    @Value("${url}")
    private String url;
    
    @Autowired
    private ProductRepository repository;
    
    @Autowired
    private ProductJsonMapper mapper;

    @Override
    public void run(String... strings) throws Exception {
        //Uses system out because this is a console application and so it is
        //appropriate to print to the main output
        System.out.println("Getting products at " + url);
        List<Product> products = repository.getProducts(url);
        JSONObject result = mapper.map(products);
        System.out.println( result.toJSONString());
    }
    
}
