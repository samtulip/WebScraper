package com.samtulip.webscraper;

import com.samtulip.webscraper.repository.Product;
import java.util.List;

/**
 * The view component. Turns a list of products into an output format.
 * @author samtulip
 */
public interface ProductMapper<T> {
    
    public T map(List<Product> products);
}
