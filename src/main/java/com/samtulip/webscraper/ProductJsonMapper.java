package com.samtulip.webscraper;

import com.samtulip.webscraper.repository.Product;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

/**
 * Takes a list of Products and turns them into JSON objects.
 * @author samtulip
 */
@Component
public class ProductJsonMapper implements ProductMapper<JSONObject> {

    @Override
    public JSONObject map(List<Product> products) {
        JSONArray array = new JSONArray();
        int total = 0;
        for( Product product: products ){
            total += product.getPrice();
            JSONObject value = new JSONObject();
            value.put("title", product.getTitle());
            value.put("description", product.getDescription());
            value.put("size", product.getSize() / 1024);
            value.put("price", (double) product.getPrice() / 100d);
            array.add( value );
        }
        JSONObject obj = new JSONObject();
        obj.put("results", array);
        obj.put("total", (double) total / 100d);
        return obj;
    }
    
    
}
