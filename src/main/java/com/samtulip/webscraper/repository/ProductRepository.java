package com.samtulip.webscraper.repository;

import java.io.IOException;
import java.util.List;

/**
 * Repository containing information about products.
 * @author samtulip
 */
public interface ProductRepository {
    
    /**
     * Gets products from a page in Sainsbury's online supermarket.
     * @param page The page you want to list products for
     * @return A list of products
     * @throws IOException If the products on the page could not be exported
     */
    public List<Product> getProducts(final String page) throws IOException;
}
