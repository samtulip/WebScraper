package com.samtulip.webscraper.repository;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * A Product that can be bought online from Sainsbury's Supermarket.
 * @author samtulip
 */

@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Product {
    
    /**
     * The title of the Product
     */
    private final String title;
    /**
     * A description of the product
     */
    private final String description;
    /**
     * The size of the page in bytes
     */
    private final int size;
    /**
     * The price of the product in pence
     */
    private final int price;
}
