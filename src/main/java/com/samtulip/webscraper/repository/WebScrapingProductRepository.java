package com.samtulip.webscraper.repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import lombok.extern.java.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Repository;

/**
 * Implementation of the ProductRepository that scrapes information off the Internet
 * @author samtulip
 */
@Repository
@Log
public class WebScrapingProductRepository implements ProductRepository {
    
    @Override
    public List<Product> getProducts(String page) throws IOException {
        log.log(Level.INFO, "Scraping {0}", page);
        /*
         * Use Jsoup to download an parse the html. An xml parser could have also
         * been used, however html is not garunteed to be valid xml so it is best
         * to use a more forgiving parser. Jsoup also has a nice selector search
         * interface.
        */
        Document document = Jsoup.connect(page).get();
        Elements productElements = document.select("div.productInfo a");
        log.log(Level.INFO, "Found {0} products", productElements.size());
        List<Product> products = new ArrayList<>();
        for( Element element: productElements) {
            Document product = Jsoup.connect( element.attr("href") ).get();
            int size = product.outerHtml().getBytes().length;
            String name = product.select("div.productTitleDescriptionContainer h1").text();
            String description = product.select("#information > productcontent > htmlcontent > div:nth-child(2) > p:nth-child(1)").text();
            String price = product.select("p.pricePerUnit").text();
            products.add( new Product(name, description, size, (int) (Double.parseDouble(price.substring(price.indexOf("£")+1, price.indexOf("/"))) * 100)));
        }
        return products;
    }
    
}
